% Q2

function [optM, logLikelihood] = EMforGMMwithCrossValidation(x,N,delta,figures, cost)
%close all,
%delta = 1; % tolerance for EM stopping criterion
regWeight = 1e-10; % regularization parameter for covariance estimates

% Generate samples from a 3-component GMM
alpha_true = [0.2,0.3,0.3,0.2];
mu_true = [-15 0 10 15;5 0 5 10];
Sigma_true(:,:,1) = [2 1;1 6];
Sigma_true(:,:,2) = [3 1;1 5];
Sigma_true(:,:,3) = [4 1;1 2];
Sigma_true(:,:,4) = [3 1;1 2];
[d,Mtrue] = size(mu_true); % determine dimensionality of samples and number of GMM components

K=10;
dummy = ceil(linspace(0,N,K+1));
for k = 1:K
    indPartitionLimits(k,:) = [dummy(k)+1,dummy(k+1)];
end

for M=1:6
    for k = 1:K
        indValidate = [indPartitionLimits(k,1):indPartitionLimits(k,2)];
        xValidate = x(:,indValidate); % Using folk k as validation set
        if k == 1
            indTrain = [indPartitionLimits(k+1,1):N];
        elseif k == K
            indTrain = [1:indPartitionLimits(k-1,2)];
        else
            indTrain = [1:indPartitionLimits(k-1,2),indPartitionLimits(k+1,1):N];
        end
        xTrain = x(:,indTrain); % using all other folds as training set
        Ntrain = length(indTrain); Nvalidate = length(indValidate);

        %M = 3;
        % Initialize the GMM to randomly selected samples
        alpha = ones(1,M)/M;
        shuffledIndices = randperm(Ntrain);
        mu = xTrain(:,shuffledIndices(1:M)); % pick M random samples as initial mean estimates
        [~,assignedCentroidLabels] = min(pdist2(mu',xTrain'),[],1); % assign each sample to the nearest mean
        for m = 1:M % use sample covariances of initial assignments as initial covariance estimates
            %alpha(1,m) = find(assignedCentroidLabels==m)/N;
            Sigma(:,:,m) = cov(xTrain(:,find(assignedCentroidLabels==m))') + regWeight*eye(d,d);
        end
        t = 0; %displayProgress(t,x,alpha,mu,Sigma);
        
        Converged = 0; % Not converged at the beginning
        while ~Converged
            for l = 1:M
                temp(l,:) = repmat(alpha(l),1,Ntrain).*evalGaussian(xTrain,mu(:,l),Sigma(:,:,l));
            end
            plgivenx = temp./sum(temp,1);
            alphaNew = mean(plgivenx,2);
            w = plgivenx./repmat(sum(plgivenx,2),1,Ntrain);
            muNew = xTrain*w';
            for l = 1:M
                v = xTrain-repmat(muNew(:,l),1,Ntrain);
                u = repmat(w(l,:),d,1).*v;
                SigmaNew(:,:,l) = u*v' + regWeight*eye(d,d); % adding a small regularization term
            end
            Dalpha = sum(abs(alphaNew-alpha'));
            Dmu = sum(sum(abs(muNew-mu)));
            DSigma = sum(sum(abs(abs(SigmaNew-Sigma))));
            Converged = ((Dalpha+Dmu+DSigma)<delta); % Check if converged
            alpha = alphaNew; mu = muNew; Sigma = SigmaNew;
            t = t+1; 
            %displayProgress(M,figures,t,x,alpha,mu,Sigma);
        end 

        LL_temp(k) = displayProgress(M,figures,t,xValidate,alpha,mu,Sigma) * (1 + cost * (M - 1));
    end
    logLikelihood(M) = mean(LL_temp);
end
[~, optM] = max(logLikelihood);
%keyboard,

%%%
function logLikelihood = displayProgress(M,figures,t,x,alpha,mu,Sigma)
if(figures)
figure(M)
if size(x,1)==2
    subplot(1,2,1), cla,
    plot(x(1,:),x(2,:),'b.'); 
    xlabel('x_1'), ylabel('x_2'), title('Data and Estimated GMM Contours'),
    axis equal, hold on;
    rangex1 = [min(x(1,:)),max(x(1,:))];
    rangex2 = [min(x(2,:)),max(x(2,:))];
    [x1Grid,x2Grid,zGMM] = contourGMM(alpha,mu,Sigma,rangex1,rangex2);
    contour(x1Grid,x2Grid,zGMM); axis equal, 
    subplot(1,2,2), 
end
end
logLikelihood = sum(log(evalGMM(x,alpha,mu,Sigma)));
if(figures)
plot(t,logLikelihood,'b.'); hold on,
xlabel('Iteration Index'), ylabel('Log-Likelihood of Data'),
drawnow; pause(0.1),
end
%%%
function x = randGMM(N,alpha,mu,Sigma)
d = size(mu,1); % dimensionality of samples
cum_alpha = [0,cumsum(alpha)];
u = rand(1,N); x = zeros(d,N); labels = zeros(1,N);
for m = 1:length(alpha)
    ind = find(cum_alpha(m)<u & u<=cum_alpha(m+1)); 
    x(:,ind) = randGaussian(length(ind),mu(:,m),Sigma(:,:,m));
end

%%%
function x = randGaussian(N,mu,Sigma)
% Generates N samples from a Gaussian pdf with mean mu covariance Sigma
n = length(mu);
z =  randn(n,N);
A = Sigma^(1/2);
x = A*z + repmat(mu,1,N);

%%%
function [x1Grid,x2Grid,zGMM] = contourGMM(alpha,mu,Sigma,rangex1,rangex2)
x1Grid = linspace(floor(rangex1(1)),ceil(rangex1(2)),101);
x2Grid = linspace(floor(rangex2(1)),ceil(rangex2(2)),91);
[h,v] = meshgrid(x1Grid,x2Grid);
GMM = (evalGMM([h(:)';v(:)'],alpha, mu, Sigma));
zGMM = reshape(GMM,91,101);
%figure(1), contour(horizontalGrid,verticalGrid,discriminantScoreGrid,[minDSGV*[0.9,0.6,0.3],0,[0.3,0.6,0.9]*maxDSGV]); % plot equilevel contours of the discriminant function 

%%%
function gmm = evalGMM(x,alpha,mu,Sigma)
gmm = zeros(1,size(x,2));
for m = 1:length(alpha) % evaluate the GMM on the grid
    gmm = gmm + alpha(m)*evalGaussian(x,mu(:,m),Sigma(:,:,m));
end

%%%
function g = evalGaussian(x,mu,Sigma)
% Evaluates the Gaussian pdf N(mu,Sigma) at each coumn of X
[n,N] = size(x);
invSigma = inv(Sigma);
C = (2*pi)^(-n/2) * det(invSigma)^(1/2);
E = -0.5*sum((x-repmat(mu,1,N)).*(invSigma*(x-repmat(mu,1,N))),1);
g = C*exp(E);
