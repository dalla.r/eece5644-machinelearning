% Q2
close all, clear all,
N = [10 100 1000 10000];
n_runs=100;
figures=0;
delta=1;
cost=0.033;

% Generate samples from a 4-component GMM
alpha_true = [0.2,0.3,0.3,0.2];
mu_true = [-10 0 0 10;0 0 -10 10];
Sigma_true(:,:,1) = [2 1;1 6];
Sigma_true(:,:,2) = [3 1;1 5];
Sigma_true(:,:,3) = [4 1;1 2];
Sigma_true(:,:,4) = [3 1;1 2];
x{1} = randGMM(N(1),alpha_true,mu_true,Sigma_true);
x{2} = randGMM(N(2),alpha_true,mu_true,Sigma_true);
x{3} = randGMM(N(3),alpha_true,mu_true,Sigma_true);
x{4} = randGMM(N(4),alpha_true,mu_true,Sigma_true);

% Plot true data
figure(1)
for i=1:4
    subplot(2,2,i), cla,
    plot(x{i}(1,:),x{i}(2,:),'b.'); 
    xlabel('x_1'), ylabel('x_2'), title("True Data and GMM Contours: " + N(i) + " samples"),
    axis equal, hold on;
    rangex1 = [min(x{i}(1,:)),max(x{i}(1,:))];
    rangex2 = [min(x{i}(2,:)),max(x{i}(2,:))];
    [x1Grid,x2Grid,zGMM] = contourGMM(alpha_true,mu_true,Sigma_true,rangex1,rangex2);
    contour(x1Grid,x2Grid,zGMM); axis equal, 
end

for n=1:length(N)
    for i=1:n_runs
        [M(n,i), LL(:,i)] = EMforGMMwithCrossValidation(x{n},N(n),delta,figures, cost);
    end
    figure(1+n),
    M_count = hist(M(n,:),1:6);
    stem([1:6],M_count);
    xlabel('M'), ylabel('GMM order rate'), title("GMM order rate using " + N(n) + " samples")
    figure(1+length(N)+n)
    bar(1:6,mean(LL,2));
end

%%%%%%%%%%%%%%%%%%%%%% Utility functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
function x = randGMM(N,alpha,mu,Sigma)
d = size(mu,1); % dimensionality of samples
cum_alpha = [0,cumsum(alpha)];
u = rand(1,N); x = zeros(d,N); labels = zeros(1,N);
for m = 1:length(alpha)
    ind = find(cum_alpha(m)<u & u<=cum_alpha(m+1)); 
    x(:,ind) = randGaussian(length(ind),mu(:,m),Sigma(:,:,m));
end
end

%%%
function x = randGaussian(N,mu,Sigma)
% Generates N samples from a Gaussian pdf with mean mu covariance Sigma
n = length(mu);
z =  randn(n,N);
A = Sigma^(1/2);
x = A*z + repmat(mu,1,N);
end

%%%
function [x1Grid,x2Grid,zGMM] = contourGMM(alpha,mu,Sigma,rangex1,rangex2)
x1Grid = linspace(floor(rangex1(1)),ceil(rangex1(2)),101);
x2Grid = linspace(floor(rangex2(1)),ceil(rangex2(2)),91);
[h,v] = meshgrid(x1Grid,x2Grid);
GMM = (evalGMM([h(:)';v(:)'],alpha, mu, Sigma));
zGMM = reshape(GMM,91,101);
%figure(1), contour(horizontalGrid,verticalGrid,discriminantScoreGrid,[minDSGV*[0.9,0.6,0.3],0,[0.3,0.6,0.9]*maxDSGV]); % plot equilevel contours of the discriminant function 
end

%%%
function gmm = evalGMM(x,alpha,mu,Sigma)
gmm = zeros(1,size(x,2));
for m = 1:length(alpha) % evaluate the GMM on the grid
    gmm = gmm + alpha(m)*evalGaussian(x,mu(:,m),Sigma(:,:,m));
end
end

%%%
function g = evalGaussian(x,mu,Sigma)
% Evaluates the Gaussian pdf N(mu,Sigma) at each coumn of X
[n,N] = size(x);
invSigma = inv(Sigma);
C = (2*pi)^(-n/2) * det(invSigma)^(1/2);
E = -0.5*sum((x-repmat(mu,1,N)).*(invSigma*(x-repmat(mu,1,N))),1);
g = C*exp(E);
end