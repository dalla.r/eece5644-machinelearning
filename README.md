# EECE5644-MachineLearning

## Machine Learning and Pattern Recognition Assignments

Professor: Dr Deniz Erdogmus

- [X] Assignment 1: principal component analysis (PCA), linear discriminant analysis (LDA), expected risk minimization (ERM)
- [X] Assignment 2: maximum likelihood (ML), maximum a posteriori (MAP), parameter estimation
- [X] Assignment 3: multilayer perceptrons (MLP) neural networks, expectation maximization (EM)
- [X] Assignment 4: tensorflow, support vector machine (SVM), GMM clustering

Assignments 1-3 are written in Matlab and tested using Matlab R2021a.
Assignment 4 is written in Python 3.8 and tested on Ubuntu 18.04.