% Q1

clear all, close all,

%% Generating samples
n = 4; % number of dimensions
N = 10000; % number samples
m(:,1) = repmat(-1,n,1); 
m(:,2) = ones(n,1);
C(:,:,1) = [2 -0.5 0.3 0; -0.5 1 -0.5 0; 0.3 -0.5 1 0; 0 0 0 2];
C(:,:,2) = [1 0.3 -0.2 0; 0.3 2 0.3 0; -0.2 0.3 1 0; 0 0 0 3];
p = [0.7,0.3]; % class priors for labels 0 and 1
labels = rand(1,N) >= p(1); % each sample is assigned a random class label
Nc = [length(find(labels==0)),length(find(labels==1))]; % number of samples from each class
x = zeros(n,N);
% Draw samples from each class pdf
for l = 0:1
    x(:,labels==l) = mvnrnd(m(:,l+1)',C(:,:,l+1),Nc(l+1))';
end
x1 = x(:,(find(labels==0)));
x2 = x(:,(find(labels==1)));
if(n==2)
    plot(x(1,labels==0),x(2,labels==0),'or'); hold on,
    plot(x(1,labels==1),x(2,labels==1),'ob');
end

% PART A: ERM classification using the knowledge of true data pdf
    
disp('Using true data pdfs');

g1 = evalGaussianPDF(x,m(:,1),C(:,:,1)); 
g2 = evalGaussianPDF(x,m(:,2),C(:,:,2));
discriminantScoresERM = log(g2./g1);
[PfpERM,PfnERM,PtpERM,PerrorERM,thresholdListERM] = ROCcurve(discriminantScoresERM,labels);
% figure(1);
% plot(thresholdListERM,PerrorERM,'.g'),
% xlabel('Thresholds'), ylabel('P(error) for ERM Discriminant Scores'),
figure(2);
plot(PfpERM,PtpERM,'.g'), axis equal, grid on,
hold on;
xlabel('P(False+)'),ylabel('P(True+)'), title('ROC Curve: True ERM'),
[min_Perror, min_Perror_ind] = min(PerrorERM);
disp("The empirically selected min Perror is " + min_Perror + " and is achieved with gamma = " + exp(thresholdListERM(min_Perror_ind)));
hold on;
min_error_x = PfpERM(min_Perror_ind);
min_error_y = PtpERM(min_Perror_ind);
plot(min_error_x, min_error_y, 'Or'), axis equal, grid on,
text_ERM = {['True ERM'],
            ["(" + min_error_x + ", " + min_error_y + ")"],
            ["ERM gamma: " + exp(thresholdListERM(min_Perror_ind))],
            ["P_{error}: " + min_Perror]};
pos = [0.4, 0.8, 0.1, 0.1];
t = annotation('textbox', pos, 'String', text_ERM);
t.LineStyle = 'none';
t.Color = 'g';
est_Perror = PfpERM * p(1) + PfnERM * p(2);
est_min_Perror = min(est_Perror);
disp("The estimated min Perror achievable is " + est_min_Perror);
    
%% PART B: Naive Bayesian ERM

%Same priors and mean but we use covariance matrices:
C_NB(:,:,1) = diag([2,1,1,2]);
C_NB(:,:,2) = diag([1,2,1,3]);

disp('--------------------');
disp('Naive Bayesian assumption');
    
g1 = evalGaussian(x,m(:,1),C_NB(:,:,1)); 
g2 = evalGaussian(x,m(:,2),C_NB(:,:,2));
discriminantScoresERM = log(g2./g1);
[PfpERM,PfnERM,PtpERM,PerrorERM,thresholdListERM] = ROCcurve(discriminantScoresERM,labels);
% figure(1);
% plot(thresholdListERM,PerrorERM,'.b'),
% xlabel('Thresholds'), ylabel('P(error) for ERM Discriminant Scores'),
figure(2);
plot(PfpERM,PtpERM,'.b'), axis equal, grid on,
hold on;
xlabel('P(False+)'),ylabel('P(True+)'), title('ROC Curve: True ERM vs NB ERM'),
[min_Perror, min_Perror_ind] = min(PerrorERM);
disp("The empirically selected min Perror is " + min_Perror + " and is achieved with gamma = " + exp(thresholdListERM(min_Perror_ind)));
hold on;
min_error_x = PfpERM(min_Perror_ind);
min_error_y = PtpERM(min_Perror_ind);
plot(min_error_x, min_error_y, 'Or'), axis equal, grid on,
text_ERM = {['NB ERM'],
            ["(" + min_error_x + ", " + min_error_y + ")"],
            ["ERM gamma: " + exp(thresholdListERM(min_Perror_ind))],
            ["P_{error}: " + min_Perror]};
pos = [0.4, 0.7, 0.1, 0.1];
t = annotation('textbox', pos, 'String', text_ERM);
t.LineStyle = 'none';
t.Color = 'b';
est_Perror = PfpERM * p(1) + PfnERM * p(2);
est_min_Perror = min(est_Perror);
disp("The estimated min Perror achievable is " + est_min_Perror);
    

%% PART C: Fisher LDA

disp('--------------------');
disp('Fisher LDA');

% Estimate mean vectors and covariance matrices from samples
mu1hat = mean(x1,2); 
S1hat = cov(x1');
mu2hat = mean(x2,2); 
S2hat = cov(x2');

% Calculate the between/within-class scatter matrices
Sb = (mu1hat-mu2hat)*(mu1hat-mu2hat)';
Sw = S1hat + S2hat;

% Solve for the Fisher LDA projection vector (in w)
[V,D] = eig(inv(Sw)*Sb);
[~,ind] = sort(diag(D),'descend');
w = V(:,ind(1)); % Fisher LDA projection vector

y1 = w'*x1; y2 = w'*x2; if mean(y2)<=mean(y1), w = -w; y1 = -y1; y2 = -y2; end,

discriminantScoresLDA = w'*x;
[PfpLDA,PfnLDA,PtpLDA,PerrorLDA,thresholdListLDA] = ROCcurve(discriminantScoresLDA, labels);

figure(2);
plot(PfpLDA,PtpLDA,'.c'), axis equal, grid on,
hold on;
xlabel('P(False+)'),ylabel('P(True+)'), title('ROC Curve: True ERM vs NB ERM vs LDA'),
[min_PerrorLDA, min_Perror_indLDA] = min(PerrorLDA);
disp("The empirically selected min Perror is " + min_PerrorLDA + " and is achieved with threshold = " + (thresholdListLDA(min_Perror_indLDA)));
hold on;
min_error_xLDA = PfpLDA(min_Perror_indLDA);
min_error_yLDA = PtpLDA(min_Perror_indLDA);
plot(min_error_xLDA, min_error_yLDA, 'Or'), axis equal, grid on,

text_LDA = {['LDA'],
            ["(" + min_error_xLDA + ", " + min_error_yLDA + ")"],
            ["LDA threshold: " + (thresholdListLDA(min_Perror_indLDA))],
            ["P_{error}: " + min_PerrorLDA]};
pos = [0.4, 0.6, 0.1, 0.1];
t = annotation('textbox', pos, 'String', text_LDA);
t.LineStyle = 'none';
t.Color = 'c';
est_PerrorLDA = PfpLDA * p(1) + PfnLDA * p(2);
est_min_PerrorLDA = min(est_PerrorLDA);
disp("The estimated min Perror achievable is " + est_min_PerrorLDA);

% figure
% plot(y1(1,:),zeros(1,Nc(1)),'r*'); hold on;
% plot(y2(1,:),zeros(1,Nc(2)),'bo');

%% Utility functions

function [Pfp,Pfn,Ptp,Perror,thresholdList] = ROCcurve(discriminantScores,labels)
[sortedScores,~] = sort(discriminantScores,'ascend');
thresholdList = [min(sortedScores)-eps,(sortedScores(1:end-1)+sortedScores(2:end))/2, max(sortedScores)+eps];
for i = 1:length(thresholdList)
    tau = thresholdList(i);
    decisions = (discriminantScores >= tau);
    Ptn(i) = length(find(decisions==0 & labels==0))/length(find(labels==0));
    Pfn(i) = length(find(decisions==0 & labels==1))/length(find(labels==1));
    Pfp(i) = length(find(decisions==1 & labels==0))/length(find(labels==0));
    Ptp(i) = length(find(decisions==1 & labels==1))/length(find(labels==1));
    Perror(i) = sum(decisions~=labels)/length(labels);
end
end