% Q3

%% Wine
clear all, close all,

data = readmatrix('./Data/Wine_Dataset/winequality-white.csv');

x = data(:,1:end-1)';
labels = data(:,end);
clear data;
n = size(x,1); %number of features
N = size(x,2); %number of samples
n_lab = 11; %number of labels
for i = 0:n_lab-1
    if ~isnan(x(:,labels==i))
        m(:,i+1) = mean(x(:,labels==i),2); %estimates means
        C(:,:,i+1) = cov(x(:,labels==i)'); %estimated covariance matrices
        lambda(i+1) = 0.25*trace(C(:,:,i+1))/rank(C(:,:,i+1)); %regularization parameter
        Nc(i+1) = length(find(labels==i));
    else
        m(:,i+1) = zeros(n_lab,1);
        C(:,:,i+1) = eye(n_lab,n_lab);
        lambda(i+1) = 0;
        Nc(i+1) = 0;
    end
end
priors = Nc/N;
for i=1:n_lab
    C(:,:,i) = C(:,:,i) + lambda(i).*eye(n,n);
end

% Classification rule (minimum probability of error)
for i = 1:n_lab
    classPosteriors(i,:) = evalGaussianPDF(x,m(:,i),C(:,:,i))*priors(i); 
end
[~, D] = max(classPosteriors, [], 1);

% Confusion Matrix
for d = 0:n_lab-1 % each decision option
    for l = 0:n_lab-1 % each class label
        ind_dl = find(D'==d & labels==l);
        ConfusionMatrix(d+1,l+1) = length(ind_dl)/length(find(labels==l));
        ConfusionMatrix(isnan(ConfusionMatrix)) = 0;
    end
end
ConfusionMatrix,

% PCA
y = PCA(x,N);
figure, clf,
mShapes = 'x+.sd^><p*o';
mColors = 'rg';
for d = 0:n_lab-1 % each decision option
    for l = 0:n_lab-1 % each class label
        plot3(y(1,:),y(2,:),y(3,:),strcat(mShapes(l+1),mColors((d==l)+1))); hold on, axis equal, grid on,
        xlabel('y_1'), ylabel('y_2'), zlabel('y_3')
        title('PCA plot of Wine Dataset classified');
    end
end

Perror_wine = 1 - length(find(D'==labels))/N,

save 'Q3.1.mat';


%% HAR

clear all, close all,

data_train = readmatrix('./Data/HAR_Dataset/train/x_train.txt');
data_test = readmatrix('./Data/HAR_Dataset/test/x_test.txt');
labels_train = readmatrix('./Data/HAR_Dataset/train/y_train.txt');
labels_test = readmatrix('./Data/HAR_Dataset/test/y_test.txt');

x = [data_train' data_test'];
labels = [labels_train' labels_test']';
n = size(x,1); %number of features
N = size(x,2); %number of samples
clear data_train, clear data_test, clear labels_train, clear labels_test,
n_lab = 6; %number of labels
for i = 1:n_lab
    if ~isnan(x(:,labels==i))
        m(:,i) = mean(x(:,labels==i),2); %estimates means
        C(:,:,i) = cov(x(:,labels==i)'); %estimated covariance matrices
        lambda(i) = 0.05*trace(C(:,:,i))/rank(C(:,:,i)); %regularization parameter
        Nc(i) = length(find(labels==i));
    else
        m(:,i) = zeros(n_lab,1);
        C(:,:,i) = eye(n_lab,n_lab);
        lambda(i) = 0;
        Nc(i+1) = 0;
    end
end
priors = Nc/N;
for i=1:n_lab
    C(:,:,i) = C(:,:,i) + lambda(i).*eye(n,n);
end

% Classification rule (minimum probability of error)

for i = 1:n_lab
    classPosteriors(i,:) = evalGaussianPDF(x,m(:,i),C(:,:,i))*priors(i); 
end
[~, D] = max(classPosteriors, [], 1);

% Confusion Matrix
for d = 1:n_lab % each decision option
    for l = 1:n_lab % each class label
        ind_dl = find(D'==d & labels==l);
        ConfusionMatrix(d,l) = length(ind_dl)/length(find(labels==l));
        ConfusionMatrix(isnan(ConfusionMatrix)) = 0;
    end
end
ConfusionMatrix,

% PCA
y = PCA(x,N);
figure, clf,
mShapes = 'x*+.o*p';
mColors = 'rg';
for d = 1:n_lab % each decision option
    for l = 1:n_lab % each class label
        plot3(y(1,:),y(2,:),y(3,:),strcat(mShapes(l),mColors((d==l)+1))); hold on, axis equal, grid on,
        xlabel('y_1'), ylabel('y_2'), zlabel('y_3')
        title('PCA plot of HAR Dataset classified');
    end
end
Perror_har = 1 - length(find(D'==labels))/N,

save 'Q3.2.mat';

%% Wine PCA individual class conditional pdfs
clear all, close all,
load Q3.1.mat;
label = 6;
y = PCA(x(:,find(labels==label)),length(find(labels==label)));
figure, clf,
mShapes = 'x+.sd^><p*o';
mColors = 'rg';
for d = 0:n_lab-1 % each decision option
    for l = 0:n_lab-1 % each class label
        plot(y(1,:),y(2,:),strcat(mShapes(l+1),mColors((d==l)+1))); hold on, axis equal, grid on,
        xlabel('y_1'), ylabel('y_2'),
        title(['PCA plot of Wine Dataset classified (label ' num2str(label) ')']);
    end
end

%% HAR PCA individual class conditional pdfs
clear all, close all,
load Q3.2.mat;
label = 4;
y = PCA(x(:,find(labels==label)),length(find(labels==label)));
figure, clf,
mShapes = 'x+.sd^><p*o';
mColors = 'rg';
for d = 1:n_lab % each decision option
    for l = 1:n_lab % each class label
        plot(y(1,:),y(2,:),strcat(mShapes(l),mColors((d==l)+1))); hold on, axis equal, grid on,
        xlabel('y_1'), ylabel('y_2'),
        title(['PCA plot of Wine Dataset classified (label ' num2str(label) ')']);
    end
end

%%
function y = PCA(x,N)
    muhat = mean(x,2);
    Sigmahat = cov(x');

    xzm = x - muhat*ones(1,N);
    [Q,D_PCA] = eig(Sigmahat);
    [d_PCA,ind] = sort(diag(D_PCA),'descend');
    Q = Q(:,ind);
    D_PCA = diag(d_PCA);

    y = Q'*xzm; %Principal components
end