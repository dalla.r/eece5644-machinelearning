% Q2

clear all, close all,

n = 3;
N = 10000;
priors = [0.3,0.3,0.4];

%% PART A: MAP classification

A = randn(n,n);
C(:,:,1) = A*A'; 
A = randn(n,n);
C(:,:,2) = A*A'; 
A = randn(n,n);
C(:,:,3) = A*A'; 
A = randn(n,n);
C(:,:,4) = A*A'; 
avg_std = det((C(:,:,1)^(1/2)+C(:,:,2)^(1/2)+C(:,:,3)^(1/2)+C(:,:,4)^(1/2))./4);
s = 2 * avg_std; %separation
m(:,1) = zeros(n,1);
m(:,2) = [s 0 0]';
m(:,3) = [0 s 0]';
m(:,4) = [0 0 s]';
x1 = randGaussian(N*priors(1), m(:,1), C(:,:,1));
x2 = randGaussian(N*priors(2), m(:,2), C(:,:,2));
x3 = randGaussian(N*priors(3)/2, m(:,3), C(:,:,3));
x4 = randGaussian(N*priors(3)/2, m(:,4), C(:,:,4));
Nc = [0.3*N,0.3*N,0.4*N];
x1(:,:,2) = ones(n,Nc(1)); % Keep track of labels
x2(:,:,2) = 2.*ones(n,Nc(2));
x3(:,:,2) = 3.*ones(n,Nc(3)/2);
x4(:,:,2) = 3.*ones(n,Nc(3)/2);
x = [x1 x2 x3 x4];
loss_MAP = 1 - eye(3);

scatter3(x1(1,:),x1(2,:),x1(3,:),'r'); hold on;
scatter3(x2(1,:),x2(2,:),x2(3,:),'g'); hold on;
scatter3(x3(1,:),x3(2,:),x3(3,:),'b'); hold on;
scatter3(x4(1,:),x4(2,:),x4(3,:),'b'); hold on;
axis equal;
title("Gaussian components");

classPosteriors(1,:) = evalGaussianPDF(x(:,:,1),m(:,1),C(:,:,1))*priors(1); % Evaluate p(x|L=l)
classPosteriors(2,:) = evalGaussianPDF(x(:,:,1),m(:,2),C(:,:,2))*priors(2); % Evaluate p(x|L=l)
classPosteriors(3,:) = evalGaussianPDF(x(:,:,1),m(:,3),C(:,:,3))*priors(3)/2 + evalGaussianPDF(x(:,:,1),m(:,4),C(:,:,4))*priors(3)/2; % Evaluate p(x|L=l)
[~, D] = max(classPosteriors, [], 1);

mShapes = 'ox*';
mColors = 'rg';
figure, clf,
for d = 1:length(priors) % each decision option
    for l = 1:length(priors) % each class label
        ind_dl = find(D==d & x(1,:,2)==l);
        ConfusionMatrix(d,l) = length(ind_dl)/length(find(x(1,:,2)==l));
        plot3(x(1,ind_dl,1),x(2,ind_dl,1),x(3,ind_dl,1),strcat(mShapes(l),mColors((d==l)+1))), hold on, axis equal, grid on,
    end
end
disp('MAP classification');
ConfusionMatrix,
title("MAP classification");


%% PART B: ERM classification

loss_ERM_10 = [0,1,10;1,0,10;1,1,0];
loss_ERM_100 = [0,1,100;1,0,100;1,1,0];

expectedRisks_10 = loss_ERM_10 * classPosteriors;
[~,D_10] = min(expectedRisks_10,[],1);
expectedRisks_100 = loss_ERM_100 * classPosteriors;
[~,D_100] = min(expectedRisks_100,[],1);

mShapes = 'ox*';
mColors = 'rg';
figure, clf,
for d = 1:length(priors) % each decision option
    for l = 1:length(priors) % each class label
        ind_dl = find(D_10==d & x(1,:,2)==l);
        ConfusionMatrix(d,l) = length(ind_dl)/length(find(x(1,:,2)==l));
        subplot(1,2,1), plot3(x(1,ind_dl,1),x(2,ind_dl,1),x(3,ind_dl,1),strcat(mShapes(l),mColors((d==l)+1))), hold on, axis equal, grid on,
    end
end
title("ERM classification case 10");
disp('---------------');
disp('ERM classification');
ConfusionMatrix,

mShapes = 'ox*';
mColors = 'rg';
for d = 1:length(priors) % each decision option
    for l = 1:length(priors) % each class label
        ind_dl = find(D_100==d & x(1,:,2)==l);
        ConfusionMatrix(d,l) = length(ind_dl)/length(find(x(1,:,2)==l));
        subplot(1,2,2), plot3(x(1,ind_dl,1),x(2,ind_dl,1),x(3,ind_dl,1),strcat(mShapes(l),mColors((d==l)+1))), hold on, axis equal, grid on,
    end
end
title("ERM classification case 100");
ConfusionMatrix,

figure,
label = 1;
y = PCA(x(:,find(x(1,:,2)==label)),length(find(x(1,:,2)==label)));
for d = 0:length(priors)-1 % each decision option
    for l = 0:length(priors)-1 % each class label
        plot(y(1,:),y(2,:),strcat(mShapes(l+1),mColors((d==l)+1))); hold on, axis equal, grid on,
        xlabel('y_1'), ylabel('y_2')
        title(['PCA plot of Q2 Dataset classified (label ' num2str(label) ')']);
    end
end

function y = PCA(x,N)
    muhat = mean(x,2);
    Sigmahat = cov(x');

    xzm = x - muhat*ones(1,N);
    [Q,D_PCA] = eig(Sigmahat);
    [d_PCA,ind] = sort(diag(D_PCA),'descend');
    Q = Q(:,ind);
    D_PCA = diag(d_PCA);

    y = Q'*xzm; %Principal components
end


