% Q2
close all, 

%% Data generation
try xTrain;
catch
    clear all
    [xTrain,yTrain,xValidate,yValidate] = hw2q2(100,1000);
    
    Ntrain = length(xTrain(1,:));
    Nvalidate = length(xValidate(1,:));

    zTrain = [xTrain(1,:).^3; xTrain(2,:).^3; 
             xTrain(1,:).^2.*xTrain(2,:); xTrain(1,:).*xTrain(2,:).^2;
             xTrain(1,:).^2; xTrain(2,:).^2;
             xTrain(1,:); xTrain(2,:);
             ones(1,Ntrain); ones(1,Ntrain)];
    zValidate = [xValidate(1,:).^3; xValidate(2,:).^3; 
            xValidate(1,:).*xValidate(2,:).^2; xValidate(1,:).^2.*xValidate(2,:);
            xValidate(1,:).^2; xValidate(2,:).^2;
            xValidate(1,:); xValidate(2,:);
            ones(1,Nvalidate); ones(1,Nvalidate)];
end

%% ML

%Train
% w_ML = pinv(zTrain*zTrain')*sum(repmat(yTrain,size(zTrain,1),1).*zTrain,2);
w_ML = pinv(zTrain*zTrain')*(yTrain*zTrain')';

%Validate
y_ML = w_ML' * zValidate;

figure(3)
plot3(xValidate(1,:),xValidate(2,:),yValidate,'.')
hold on, axis equal,
plot3(xValidate(1,:),xValidate(2,:),y_ML,'.');
legend("Data","ML estimate");
xlabel('x1'),ylabel('x2'), zlabel('y'),
title("ML estimation on validate dataset")


%% MAP

%Train
gamma=[linspace(10^(-4),1,length(yValidate)/2), linspace(1,10^(4),length(xValidate)/2)];
for i=1:length(gamma)
    w_MAP(:,:,i) = pinv(zTrain*zTrain'+eye(size(zTrain,1))./gamma(i))*(yTrain*zTrain')';
end

%Validate
for i=1:length(gamma)
    y_MAP(:,:,i) = w_MAP(:,:,i)' * zValidate;
end

%% Error

error_ML = mean((yValidate-y_ML).^2);
for i=1:length(gamma)
    errors_MAP(i) = mean((yValidate-y_MAP(:,:,i)).^2);
end
[error_MAP_min, gamma_idx_min] = min(errors_MAP);
[error_MAP_max, gamma_idx_max] = max(errors_MAP);
gamma_min_MAP = gamma(gamma_idx_min);

figure(4)
semilogx(linspace(10^-4,10^4,length(errors_MAP)),repmat(error_ML,1,length(errors_MAP)));
hold on,
semilogx(linspace(10^-4,10^4,length(errors_MAP)), errors_MAP);
xlabel('γ'),ylabel('error'),
legend("ML","MAP");
title("Average squared error");

%% Plot MAP
figure(3)
hold on, axis equal,
plot3(xValidate(1,:),xValidate(2,:),y_MAP(:,:,gamma_idx_min),'.');
legend("Data","ML estimate","MAP estimate");
title("ML vs MAP estimation on validate dataset")

